# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import pytz
import xmlrpclib

from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.forms import AuthenticationForm
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.conf import settings
from django.http import (
  HttpResponseBadRequest,
  HttpResponseForbidden,
  HttpResponseRedirect,
)
from django.shortcuts import get_object_or_404, render
from django.utils.translation import ugettext as _

from malias.models import Alias, Domain, Email


class MyAuthenticationForm(AuthenticationForm):
    """
    Override the default AuthenticationForm in order to add HTML5 attributes.
    This is the only change done and needed
    """
    def __init__(self, *args, **kwargs):
        super(MyAuthenticationForm, self).__init__(*args, **kwargs)
        # Add HTML5 attributes
        self.fields['username'].widget.attrs['placeholder'] = _('username')
        self.fields['username'].widget.attrs['autofocus'] = 'autofocus'
        self.fields['username'].widget.attrs['required'] = 'required'
        self.fields['password'].widget.attrs['placeholder'] = _('password')

        self.fields['username'].widget.attrs['class'] = 'form-control'
        self.fields['password'].widget.attrs['class'] = 'form-control'


def home(request):
    return render(request, "malias/home.html")


@login_required
def domains_index(request):
    if request.user.is_superuser:
        domains = Domain.objects.all()
    else:
        domains = Domain.objects.filter(email__user=request.user)
    return render(request, "malias/domains/index.html", {"domains": domains})


@login_required
def domains_details(request, fqdn):
    domain = get_object_or_404(Domain, fqdn=fqdn)
    if not domain.is_visible_to(request.user):
        return HttpResponseForbidden()

    return render(request, "malias/domains/details.html", {"domain": domain})


@user_passes_test(lambda u: u.is_superuser)
def domains_import(request):
    # TODO: add try/except
    # SSLEOFError
    api = xmlrpclib.ServerProxy("https://rpc.gandi.net/xmlrpc/", use_datetime=True)
    domains = api.domain.list(settings.API_KEY)
    for domain in domains:
        # TODO: save expiration date
        d, _ = Domain.objects.get_or_create(fqdn=domain["fqdn"])
        d.expiration = pytz.timezone("UTC").localize(domain["date_registry_end"], is_dst=None)
        d.save()

    return HttpResponseRedirect(reverse("domains.index"))


@user_passes_test(lambda u: u.is_superuser)
def domains_import_emails(request, fqdn):
    domain = get_object_or_404(Domain, fqdn=fqdn)
    # TODO: add try/except
    # SSLEOFError
    api = xmlrpclib.ServerProxy("https://rpc.gandi.net/xmlrpc/")
    emails = api.domain.mailbox.list(settings.API_KEY, domain.fqdn)
    for data in emails:
        email = "%s@%s" % (data["login"], domain)
        try:
            e = Email.objects.get(domain=domain, email=email)
        except Email.DoesNotExist:
            e = Email.objects.create(domain=domain, email=email, user=request.user)
        e.used = data["quota"]["used"] * 1024
        e.quota = data["quota"]["granted"] * 1024
        e.save()

    return HttpResponseRedirect(reverse("domains.index"))


@login_required
def emails_index(request):
    accounts = Email.objects.filter(user=request.user)
    return render(request, "malias/emails/index.html", {'accounts': accounts})


@login_required
def emails_details(request, email):
    account = get_object_or_404(Email, email=email)
    if request.user != account.user and not request.user.is_superuser:
        return HttpResponseForbidden()
    return render(request, "malias/emails/details.html", {'account': account})


@login_required
def emails_details_import(request, email):
    # TODO: Make a "sync" method instead that import and then export
    account = get_object_or_404(Email, email=email)
    if request.user != account.user and not request.user.is_superuser:
        return HttpResponseForbidden()
    login, domain = account.email.split('@')

    # TODO: add try/except
    # SSLEOFError
    api = xmlrpclib.ServerProxy("https://rpc.gandi.net/xmlrpc/")
    info = api.domain.mailbox.info(settings.API_KEY, domain, login)

    # Save quota
    account.used = info["quota"]["used"] * 1024
    account.quota = info["quota"]["granted"] * 1024

    # Add missing aliases
    local_aliases = set(account.aliases.all().values_list('name', flat=True))
    remote_aliases = set(info["aliases"])
    intersection = local_aliases & remote_aliases

    for name in remote_aliases - intersection:
        alias, created = Alias.objects.get_or_create(name=name, domain=account.domain)
        account.aliases.add(alias)

    account.save()
    return HttpResponseRedirect(reverse("emails.details", args=[account]))


@login_required
def emails_aliases_add(request, email):
    account = get_object_or_404(Email, email=email)
    if request.method != "POST":
        return HttpResponseBadRequest()

    alias_name = request.POST.get("alias")
    if alias_name is None:
        return HttpResponseBadRequest()

    alias, _ = Alias.objects.get_or_create(name=alias_name, domain=account.domain)
    # TODO: print a message
    account.aliases.add(alias)

    aliases = [a.name for a in account.aliases.all()]
    api = xmlrpclib.ServerProxy("https://rpc.gandi.net/xmlrpc/", use_datetime=True)
    ret = api.domain.mailbox.alias.set(settings.API_KEY, account.domain.fqdn,
                                       account.username(), aliases)

    return HttpResponseRedirect(reverse("emails.details", args=[account.email]))


@login_required
def emails_aliases_del(request, email):
    account = get_object_or_404(Email, email=email)
    if request.method != "POST":
        return HttpResponseBadRequest()

    alias_name = request.POST.get("alias")
    if alias_name is None:
        return HttpResponseBadRequest()

    alias = get_object_or_404(Alias, name=alias_name)
    # TODO: print a message
    account.aliases.remove(alias)

    aliases = [a.name for a in account.aliases.all()]
    api = xmlrpclib.ServerProxy("https://rpc.gandi.net/xmlrpc/", use_datetime=True)
    ret = api.domain.mailbox.alias.set(settings.API_KEY, account.domain.fqdn,
                                       account.username(), aliases)

    return HttpResponseRedirect(reverse("emails.details", args=[account.email]))


@login_required
def emails_send(request, email, alias):
    email = get_object_or_404(Email, email=email)
    alias = get_object_or_404(Alias, name=alias)
    # All emails are from the same domain so take the first one
    domain = alias.domain.fqdn
    alias_email = "%s@%s" % (alias.name, domain)
    from_email = "malias@%s" % domain
    send_mail("[malias] Testing alias",
              "Hello,\nthis mail has been sent to %s in order to test that the alias is working.\n\n-- \nMalias" % alias_email,
              from_email, [alias_email], fail_silently=False)
    return HttpResponseRedirect(reverse("emails.details", args=[email]))
