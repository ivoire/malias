from __future__ import unicode_literals

from django.conf.urls import include, url
from django.contrib.auth import views as m_auth

import malias.views as m_views

urlpatterns = [
    url(r'^$', m_views.home, name='home'),

    # accounts
    url(r'^accounts/login/$', m_auth.login, {'template_name': 'malias/account/login.html', 'authentication_form': m_views.MyAuthenticationForm}, name='accounts.login'),
    url(r'^accounts/logout/$', m_auth.logout, {'template_name': 'malias/account/logged_out.html'}, name='accounts.logout'),

    # domains
    url(r'^domains/$', m_views.domains_index, name='domains.index'),
    url(r'^domains/import/$', m_views.domains_import, name='domains.import'),
    url(r'^domains/(?P<fqdn>[\w_.-]+)/$', m_views.domains_details, name='domains.details'),
    url(r'^domains/(?P<fqdn>[\w_.-]+)/import/$', m_views.domains_import_emails, name='domains.import.emails'),

    # emails
    url(r'^emails/$', m_views.emails_index, name='emails.index'),
    url(r'^emails/(?P<email>[\w_.@-]+)/$', m_views.emails_details, name='emails.details'),
    url(r'^emails/(?P<email>[\w_.@-]+)/aliases/add/$', m_views.emails_aliases_add, name='emails.aliases.add'),
    url(r'^emails/(?P<email>[\w_.@-]+)/aliases/delete/$', m_views.emails_aliases_del, name='emails.aliases.delete'),
    url(r'^emails/(?P<email>[\w_.@-]+)/send/(?P<alias>[\w._-]+)/$', m_views.emails_send, name='emails.send'),
    url(r'^emails/(?P<email>[\w_.@-]+)/import/$', m_views.emails_details_import, name='emails.details.import'),
]
