# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from malias.models import Alias, Domain, Email


class AliasAdmin(admin.ModelAdmin):
    list_filter = ('domain', 'emails')
    list_display = ('name', 'domain')


class DomainAdmin(admin.ModelAdmin):
    list_filter = ('fqdn', )
    list_display = ('fqdn', 'expiration')


class EmailAdmin(admin.ModelAdmin):
    list_display = ('email', 'domain', 'user', 'used', 'quota')


admin.site.register(Alias, AliasAdmin)
admin.site.register(Domain, DomainAdmin)
admin.site.register(Email, EmailAdmin)
