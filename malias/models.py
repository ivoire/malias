# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import User, Group
from django.core.validators import MinValueValidator
from django.db import models
from django.utils import timezone


class Domain(models.Model):
    fqdn = models.CharField(primary_key=True, max_length=200)
    expiration = models.DateTimeField(default=timezone.now)

    class Meta:
        ordering = ["fqdn"]

    def __str__(self):
        return self.fqdn

    def is_visible_to(self, user):
        if user.is_superuser:
            return True
        else:
            return self.email_set.filter(user=user).exists()


class Alias(models.Model):
    name = models.CharField(blank=False, max_length=200)
    domain = models.ForeignKey(Domain, blank=False, related_name="+")

    class Meta:
        ordering = ["domain__fqdn", "name"]
        verbose_name_plural = "Aliases"

    def __str__(self):
        return "%s@%s => %s" % (self.name, self.domain.fqdn, ", ".join([email.username() for email in self.emails.all()]))


class Email(models.Model):
    email = models.EmailField(primary_key=True)
    domain = models.ForeignKey(Domain, blank=False, on_delete=models.CASCADE)
    aliases = models.ManyToManyField(Alias, blank=True, related_name="emails")
    user = models.ForeignKey(User, blank=False, on_delete=models.CASCADE,
                             related_name="email_account")
    quota = models.BigIntegerField(blank=False, default=1024*1024*1024,
                                   validators=[MinValueValidator(1)],
                                   help_text='Quota in Bytes')
    used = models.BigIntegerField(blank=False, default=0,
                                  validators=[MinValueValidator(0)],
                                  help_text='Size used in Bytes')

    class Meta:
        ordering = ["domain__fqdn", "email"]

    def __str__(self):
        return self.email

    def usage(self):
        return int(float(self.used) / self.quota * 100)

    def username(self):
        return self.email.split("@")[0]
